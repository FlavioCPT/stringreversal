/*************************************************************************************************
 *  Name:Flavio Palacios Tinoco                                                                  *
 *  Assignement: Programming Assignment 1(Part 1)                                                *
 *  Description:                                                                                 *
 *      Write a Java method that will take a string and reverse it. Can you come up with a       *
 *      method that is faster than regular reversing of the for loop?                            *
 *                                                                                               *
 *      Example #1:                                                                              *
 *      Input: ‘Hello World’                                                                     *
 *      Output: ‘dlroW olleH’                                                                    *
 *************************************************************************************************/

//Here the appropriate library is loaded to accept keyboard input.
import java.util.Scanner;

/******************************
    CLASS: Reverses a String
 ******************************/
public class reverseString{
    public static void main(String[ ] arg)
    {
        //"givenString" is a string variable.
        String givenString;

        //The "scan" variable will hold keyboard input.
        //The final line in this block features "scan"
        //passing its contents to the "givenString" variable.
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a String: ");
        givenString = scan.nextLine();

        //"strLength" stores the length of the "givenSting" by using the
        //".length()" library function.
        System.out.println("Reverse of a String '" + givenString + "' is:");
        int strLength = givenString.length();

        //Since we know how long "givenSting" is, print every character in it
        //beginning with the final character, proceeding with the previous
        //character, and finishing with the first character.
        while(strLength > 0)
        {
            System.out.print(givenString.charAt(strLength - 1));
            strLength--;
        }
    }
}

/*#################################################################################################
                                          EXAMPLE OUTPUT
                                       --------------------
(Example 1)
Enter a string: TACOCAT
Reverse of a String 'TACOCAT' is:
TACOCAT

(Example 2)
Enter a String: I like Chocolate
Reverse of a String 'I like Chocolate' is:
etalocohC ekil I

(Example 3)
Enter a String: Watermelon 12345 &+_? kj$51
Reverse of a String 'Watermelon 12345 &+_? kj$51' is:
15$jk ?_+& 54321 nolemretaW
#################################################################################################*/
